#https://www.practo.com/delhi/doctor/dr-a-k-grover-ophthalmologist-2

from bs4 import BeautifulSoup

def detailParser(detail_input):
    url = detail_input["url"]
    html_dump = detail_input["html"]
    soup = BeautifulSoup(html_dump, 'lxml')
    name = (soup.find_all("div", class_="doctor-details-wrapper")[0]).find('h1').get_text()
    qualification = ' '.join(soup.find("p", class_="doctor-qualifications").get_text().strip().split())
    specialities = soup.find("h2", class_="doctor-specialties")
    experience = specialities.get_text().split(',')[-1].strip()
    #speciality = ' '.join(specialities.get_text().split(',')[:-1]).strip().replace('\n', ', ').replace('\t','').replace('  ', '')
    #speciality = specialities.find('a').get_text()
    meta_list = specialities.find_all('meta')
    link_list = specialities.find_all('a')
    if len(meta_list) != 0:
        speciality = ', '.join([x["content"] for x in meta_list])
    else:
        speciality = ', '.join([x.get_text() for x in link_list])

    address_blocks = soup.find_all('div', class_="clinic-block listing-row")
    addresses = []
    print url
    for i in address_blocks:
        this_address = {}
        this_address['locality'] = i.find('div', class_="clinic-locality").get_text().split('\n')[1].replace(',','').strip()
        this_address['city'] = i.find('div', class_="clinic-locality").get_text().split('\n')[2].replace('\t','').strip()
        this_address['address'] = i.find('div', class_="clinic-address").get_text().replace('\n', '').replace(', DelhiView map','')\
            .replace(', GurgaonView map', '').replace(', NoidaView map', '')
        this_address['timings'] = ''
        try:
            this_address['timings'] = i.find('div', class_="clinic-timings-wrapper").get_text().replace('\t','').replace('\n\n\n',' ')\
            .replace('\n\n', '')
        except:
            pass
        this_address["fees"] = ''
        try:
            this_address["fees"] = i.find('div', class_="clinic-fees").get_text().split("\n\t")[0].strip()
        except:
            pass
        addresses.append(this_address)
    services = [x.get_text().replace('\n', '').replace('\t','') for x in soup.find_all('div', class_="service-cell")]
    resp_object = {}
    resp_object["name"] = name
    resp_object["qualification"] = qualification
    resp_object["speciality"] = speciality
    resp_object["addresses"] = addresses
    resp_object["services"] = services
    resp_object["id"] = str(detail_input["id"])
    resp_object["url"] = url
    return resp_object



