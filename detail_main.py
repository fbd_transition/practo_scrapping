import csv
from persisting import findDetailPage, dumpDetailPage
from crawler import browse, br
import time

#read deatil page url file
temp = open('listing_ghaziabad_dentist.csv', 'rU')
temp1 = csv.DictReader(temp)
for i in temp1:
    url = i["detail_url"]
    #check if this url is already crawled
    if findDetailPage(url) != None:
        print url
        continue
    try:
        print url
        html = browse(br, url)
        print dumpDetailPage(url, html)
    except Exception as e:
        print e
    time.sleep(5)
temp.close()