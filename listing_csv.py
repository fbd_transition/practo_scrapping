"""
In this file we will parse listing data which is already stored in mongodb
"""
from persisting import findPage
from parserListing import getListingData
import csv

city_name = 'ghaziabad'

#base_url = 'https://www.practo.com/this_city/'

temp = open('dentists.txt', 'r')
temp1 = temp.readlines()
city_urls = []
for i in temp1:
    city_url = i.strip().replace('this_city',city_name)
    city_urls.append(city_url)
temp.close()

#search this url regex in mongodb
ofile = open('listing_ghaziabad_dentist.csv', 'wb')
writer = csv.writer(ofile, delimiter=',', quoting=csv.QUOTE_ALL, quotechar='"')
header_listing = ['name', 'doc_specialities', 'qualification', 'exp_years', 'listing_locality', 'listing_fees', 'detail_url']
writer.writerow(header_listing)
for city_url in city_urls:
    print city_url
    for i in findPage(city_url):
        for m in getListingData({"html":i["html"], "url":i["url"]}):
            writer.writerow([m['name'], m['doc_specialities'], m['qualification'], \
                             m['exp_years'], m['listing_locality'], m['listing_fees'], m['detail_url']])
ofile.close()
