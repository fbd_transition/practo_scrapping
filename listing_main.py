from crawler import browse, br
from parserListing import getPaginator, getNextURL
from persisting import dumpPage
import time


city_value = 'ghaziabad'

temp = open('dentists.txt', 'r')
temp1 = temp.readlines()
for i in temp1:
    seed_url = i.strip().replace('this_city', city_value)
    print seed_url
    html = browse(br, seed_url)
    print dumpPage(seed_url, html)
    nextURLResp = getNextURL(seed_url, getPaginator(html))
    while nextURLResp["status"]:
        time.sleep(3)
        print nextURLResp["next_url"]
        html = browse(br, nextURLResp["next_url"])
        print dumpPage(nextURLResp["next_url"], html)
        nextURLResp = getNextURL(seed_url, getPaginator(html))

temp.close()

