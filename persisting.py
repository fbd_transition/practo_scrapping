from pymongo import MongoClient

client = MongoClient('localhost',27017)
practo = client.practo
html_dumps = practo.html_dumps
detail_dumps = practo.detail_dumps


def dumpPage(url, html):
    return html_dumps.insert({"url":url, "html":html})

def findPage(url_pattern):
    all_pages = html_dumps.find({"url":{"$regex":url_pattern}})
    resp_object = [{"url": x["url"], "html":x["html"]} for x in all_pages]
    return resp_object

def dumpDetailPage(url, html):
    return detail_dumps.insert({"url":url, "html":html})

def findDetailPage(url):
    return detail_dumps.find_one({"url":url})

def findDetailHtml(url_pattern):
    all_pages = detail_dumps.find({"url":{"$regex":url_pattern}})
    resp_object = [{"url": x["url"], "html":x["html"], "id":x["_id"]} for x in all_pages]
    return resp_object