from bs4 import BeautifulSoup


#div
#page-link-container

def getPaginator(html):
    soup = BeautifulSoup(html, 'lxml')
    search_resp = soup.find_all("a", class_="page_link")
    pageObject = []
    for i in search_resp:
        this_row = {}
        this_row['link'] = i.get('href')
        this_row['text'] = i.get_text()
        pageObject.append(this_row)
    return pageObject

def getNextURL(seedURL, pageObject):
    #get href from next page tag
    isNext = False
    for i in pageObject:
        if 'NEXT' in i['text']:
            isNext = True
            return {"next_url":seedURL+i['link'], "status":isNext}
    return {"status":isNext, "next_url":None}

def getListingData(listing_input):
    html_dump = listing_input["html"]
    url = listing_input["url"]

    soup = BeautifulSoup(html_dump, 'lxml')
    search_resp = soup.find_all("div", class_="listing-row fm-container")
    listings = []
    for i in search_resp:
        this_row = {}
        doc_details = i.find("div", class_="doc-details-block")
        this_row['detail_url'] = doc_details.find('a').get('href')
        this_row['name'] = doc_details.find('h2').get_text().strip()
        qualification_block = doc_details.find('p', class_="doc-qualifications")
        this_row['qualification'] = ', '.join([x.get_text().strip() for x in qualification_block.find_all("span")])
        this_row['exp_years'] = ', '.join([x.get_text().strip() for x in doc_details.find_all('p', class_="doc-exp-years")])
        this_row['doc_specialities'] = ', '.join([x.get_text().strip() for x in doc_details.find_all('p', class_="doc-specialties ")])

        doc_avail = i.find("div", class_="doc-availability-block")

        this_row['listing_locality'] = doc_avail.select('span[itemprop="addressLocality"]')[0].get_text()

        this_row['listing_fees'] = ', '.join([x.get_text().strip() for x in doc_avail.select('span[class="fees-amount"]')])

        listings.append(this_row)
    return listings
