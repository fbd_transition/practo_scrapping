from persisting import findDetailHtml
from parserDetail import detailParser

import csv

city_name = 'ghaziabad'

base_url = 'https://www.practo.com/this_city/'

city_url_pattern = base_url.replace('this_city',city_name)




'''
{'addresses': [{'fees': '', 'city': u'Gurgaon',
'address': u'Aarvy Hospital 530/18, Civil Lines, Landmark : Opposite To Nehru Stadium',
'timings': u'MON - SAT 9:00  AM - 9:00  PM', 'locality': u'Civil Lines'}],
'speciality': u'Urologist, , , , , Andrologist', 'qualification': u'MBBS , MS - General Surgery , MCh - Urology',
 'services': [], 'id': '5851a84de4eef151ce4564cd', 'name': u'Dr. Pradeep Bansal'}
'''

ofile = open('ghaziabad_speciality.csv', 'wb')
writer = csv.writer(ofile, delimiter=',', quoting =csv.QUOTE_ALL, quotechar='"')
writer.writerow(['name', 'address', 'locality', 'city', 'timings', 'fees', 'speciality', 'qualification', 'services', 'url', 'id'])
for i in findDetailHtml(city_url_pattern):
    resp_object = detailParser(i)
    name = resp_object['name']
    id = resp_object["id"]
    url = resp_object["url"]
    qualification = resp_object["qualification"]
    speciality = resp_object["speciality"]
    services = '|'.join(resp_object["services"])
    for j in resp_object["addresses"]:
        fees = j["fees"]
        address = j["address"]
        timings = j["timings"]
        locality = j["locality"]
        city = j["city"]
        final_array = [name, address, locality, city, timings, fees, speciality, qualification, services, url, id]
        final_array = [x.encode('utf-8').strip() for x in final_array]
        writer.writerow(final_array)
ofile.close()



